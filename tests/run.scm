(use cell test)

(test-begin)

(test-group "Convenience syntax"

  (define a (cell))
  (define b (cell 2))
  (define c (cell 3))

  (define sum
    (cell (a b c)
          (+ a b c)))

  (define got-avg? #f)

  (define avg
    (cell (sum)
          (set! got-avg? #t)
          (/ sum 3)))

  (test-assert (not (cell-bound? a)))
  (test-assert (cell-bound? b))
  (test-assert (cell-bound? c))
  (test-assert (not (cell-bound? sum)))
  (test-assert (not (cell-bound? avg)))

  (test-error (cell-value a))
  (test 2 (cell-value b))
  (test 3 (cell-value c))
  (test-error (cell-value sum))
  (test-error (cell-value avg))

  (update-cell! a 1)

  (test-assert got-avg?)
  (test 1 (cell-value a))
  (test 6 (cell-value sum))
  (test 2 (cell-value avg)))

(test-end)

(test-exit)

