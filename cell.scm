(module cell

(cell
 make-cell
 make-input-cell
 update-cell!
 cell-bound?
 cell-value)

(import chicken scheme)
(use lolevel srfi-1)

(define-record-type cell
  (%make-cell children inputs bound? value update)
  cell?
  (children cell-children cell-children-set!)
  (inputs cell-inputs)
  (value cell-value-ref cell-value-set!)
  (bound? cell-bound? cell-bound!)
  (update cell-update))

(define-record-printer (cell cell out)
  (if (cell-bound? cell)
      (begin
        (display "#<cell " out)
        (write (cell-value-ref cell) out)
        (display ">" out))
      (display "#<unbound-cell>" out)))

(define (bind-cell! cell new-value)
  (cell-value-set! cell new-value)
  (cell-bound! cell #t)
  (update-children! cell))

(define (try-update! cell)
  (when (every cell-bound? (cell-inputs cell))
    (let ((values (map cell-value-ref (cell-inputs cell))))
      (bind-cell! cell (apply (cell-update cell) values)))))

(define (update-children! cell)
  (let ((children (filter locative->object (cell-children cell))))
    (for-each (lambda (cell*)
                (try-update! (locative->object cell*)))
              children)
    (cell-children-set! cell children)))

(define (add-child! cell child)
  (cell-children-set! cell (cons (make-weak-locative child) (cell-children cell))))

(define (make-cell inputs update)
  (let ((cell (%make-cell (list) inputs #f #f update)))
    (for-each (lambda (input)
                (add-child! input cell))
              inputs)
    (try-update! cell)
    cell))

(define (make-input-cell . value)
  (let* ((bound? (pair? value))
         (value  (and bound? (car value))))
    (%make-cell (list) (list) bound? value #f)))

(define (update-cell! cell value)
  (unless (null? (cell-inputs cell))
    (error 'update-cell! "Not an input cell" cell))
  (bind-cell! cell value))

(define (cell-value cell)
  (if (cell-bound? cell)
      (cell-value-ref cell)
      (error 'cell-value "Unbound cell" cell)))

(define-syntax cell
  (syntax-rules ()
    ((cell)
     (make-input-cell))
    ((cell value)
     (make-input-cell value))
    ((cell (input ...) body ...)
     (make-cell (list input ...)
                (lambda (input ...)
                  body ...)))))

)
